# CdSite - Site officiel du cercle des sciences

L'ensemble de ce repo a pour but de mettre en place de façon complete le site du cercle des sciences accompagné de différentes fonctionnalités pouvant être nécessaires

## Getting Started

L'ensembl du projet se base sur les templates Bootsrap pour le css et sql pour les requêtes en bdd. Nous avons pris comme point de départ le site existant et se trouvant dans le dossier "oldSite".

## Bugs and Issues

Tous les bugs et les tâches en cours sont trouvables dans les issues. Dans le cas ou il venait à en manquer une ou si nous découvront un bug, il est intéressant d'ouvrir une nouvelle issue pour le signaler.

## Creator

Ce projet est maintenu durant l'année 2018 - 2019 par : 

- Miguel Terol Espino
- Noëmie Muller

Ce projet etait maintenu durant l'année 2017 - 2018 par : 

- Nathan Liccardo
- Miguel Terol
- Winter Bui

## Copyright and License

Le site web ainsi que les informations s'y trouvant sont la propriété du cercle des sciences (CdS - ASBL).
