<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("pages/include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("pages/include/navbar.php"); ?>

    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12 text-center">
                    <div id="carousel-example-generic" class="carousel slide">
                        <!-- Indicators -->
                        <ol class="carousel-indicators hidden-xs">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <a href="/pages/events.php?#jobday">
                                    <img class="img-responsive img-full" src="img/slides/slide-0.jpg" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="/pages/events.php?#nds">
                                    <img class="img-responsive img-full" src="img/slides/slide-1.jpg" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="/pages/events.php?#gazon">
                                    <img class="img-responsive img-full" src="img/slides/slide-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="/pages/events.php?#kmUlb">
                                    <img class="img-responsive img-full" src="img/slides/slide-3.jpg" alt="">
                                </a>
                            </div>
                            <div class="item">
                                <a href="/pages/events.php?#ffsb">
                                    <img class="img-responsive img-full" src="img/slides/slide-4.jpg" alt="">
                                </a>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="icon-next"></span>
                        </a>
                    </div>
                    <h2 class="brand-before">
                        <small>Bienvenue sur le site du </small>
                    </h2>
                    <h1 class="brand-name">CdS !</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Qu'est-ce que le 
                        <strong>Cercle des sciences</strong>
                    </h2>
                    <hr>
                    <img class="img-responsive img-border img-left" src="img/images/atom.png" alt="">
                    <hr class="visible-xs">
                    <p>Fondé il y a maintenant plus de 125 ans, le Grandissime Cercle des Sciences a pour but premier de rassembler les étudiants de la Faculté des Sciences.</p>

                    <p>Le Cercle se veut à destination de tous et c’est dans cette optique qu’au fil des années, il se diversifie pour offrir des activités tant culturelles, philanthropiques et sportives, que festives et folkloriques.</p>

                    <p>Plus que jamais concernés par les sciences et investis dans la recherche comme en témoignent les 10 km de l’ULB, nous mettons également un point d’honneur à l’accompagnement de l’étudiant lors de tout son cursus. Que ce soit dès son arrivée avec le parrainage social, et même avant lors de l’accueil facultaire, ou en fin de parcours avec des ateliers CV et le Jobday Sciences, notre grandissime famille restera un lieu de partage, d’apprentissage et de convivialité.</p>

                    <p>Toujours dans l’idée de promouvoir notre domaine d’études, nous organisons aussi le Festival du Film Scientifique de Bruxelles en diffusant du contenu accessible aux plus jeunes et, qui sait, peut-être leur donner goût au monde scientifique.</p>

                    <p>Bien sûr, le CdS reste également un lieu de détente et de fête, avec des soirées de tout type, autant que le permet notre agenda fort chargé.</p>

                    <p>Un lieu de folklore aussi. En tant que cercle folklorique, nous organisons un baptême qui se veut réfléchi et prêt à évoluer si le besoin s’en fait ressentir, tout en maintenant les traditions et les valeurs qui lui sont chères.</p>

                    <p>Fier de sa place parmi les autres cercles et de l’influence qu’il a sur ces derniers, le Cercle des Sciences se force à insuffler progrès et dépassement, en maintenant actif le feu qui anime la passion de tous les amis de la Science.</p>

                    <p><strong>Qu’il vive de très nombreuses années encore et fournisse à jamais joie, apprentissage et motivation à tous ses acteurs.</strong></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">informations 
                        <strong>Académiques</strong>
                    </h2>
                    <hr>
                    <p>Depuis toujours, le Cercle des Sciences est un subtil mélange, parfaitement dosé, entre folklore et études. C'est pourquoi nous proposons bon nombre d'activités aussi bien culturelles que sportives.</p>
                    <p>L'aide aux nouveaux étudiants de BA1 fait donc également partie intégrante de nos priorités. Plusieurs initiatives sont en effet mises en place pour les aider à affronter et réussir leur première année dans les meilleurs conditions possibles.</p>
                    <p><strong>Consultez la page académique afin d'obtenir plus d'infos !</strong></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Dates 
                        <strong>Evenements </strong>
                    </h2>
                    <hr>
                    <p>Les dates de nos divers évènements seront régulièrement mises à jour afin de pouvoir suivre et connaître nos activités. Nous posterons en temps voulu les informations et dates du bal (Nuit des Sciences), du FFSB, des 10 km, du Forum de l'Emploi, du Gazon mais également de l'ensemble des évènements folkloriques organisés par ou en collaboration avec le CdS. N'hésitez pas à consulter notre site régulièrement !</p>
                    <p>Cet hiver, nous posterons également l'ensemble des modalités et informations concernant le ski organisé par le cercle en collaboration avec Proride.</p>
                    <p>Pour finir, nous postons également chaque semaine la carte proposée par la cellule Bouffe-Ambiance afin de pouvoir préparer à l'avance votre estomac !</p>
                    <p><strong>L'ensemble des ces dates et informations se trouvent sous l'onglet Agenda de notre site.</strong></p>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <?php include("pages/include/footer.php"); ?>

</body>

<script src="js/jquery-1.11.2.min.js"></script>

</html>
