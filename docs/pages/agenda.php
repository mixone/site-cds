<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("include/navbar.php"); ?>

    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Notre 
                        <strong>Agenda !</strong>
                    </h2>
                    <hr>
                    <div class="visible-phone responsive-iframe-container">
                    <iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showDate=0&amp;showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;hl=fr&amp;bgcolor=%23eeebef&amp;src=utchocln38rcsri9oh04hn85es%40group.calendar.google.com&amp;color=%23ffcf03&amp;ctz=Europe%2FBrussels" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <?php include("include/footer.php"); ?>

</body>

</html>
