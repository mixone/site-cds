<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("include/navbar.php"); ?>

    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Activités 
                        <strong>Académiques</strong>
                    </h2>
                    <hr>
                    <p>Chaque année, l'ULB accueille des centaines de nouveaux étudiants. Bon nombre de ceux-ci se retrouvent parfois perdus ou seuls dans l'immensité de notre université. Etant donné sa vocation académique et sociale, le Cercle des Sciences met également à la disposition des nouveaux étudiants un certain nombre d'aides et de services leur permettant de rentrer de la plus belle des façons dans ce nouveau monde.</p>
                    <p>Si vous êtes nouvel étudiant en sciences et que vous cherchez de l'aide ou des renseignements : vous êtes au bon endroit. Vous retrouverez ici l'entièreté des activités et initiatives prises par le cercle afin de vous rendre la vie plus facile !</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">La 
                        <strong>JANE & Accueil</strong>
                    </h2>
                    <hr>
                    <p>JANE ou « Journée d’Accueil des Nouveaux Étudiants », est une étape cruciale de la rentrée. Les nouveaux étudiants, venus en masse découvrir ce nouvel univers, sont assaillis de toutes parts par les associations de l’ULB. Savoir se montrer original et attirer le regard est devenu la spécialité du CdS : stand, flyers, magazines, costumes, packs cadeaux de bienvenue, rien ne manque !</p>
                    <p>Au Stand, nous répondons aux questions, distribuons le journal du cercle « spécial rentrée » (contenant des mots de professeurs, du président, des délégués de section, un plan du Solbosch et de la Plaine, etc.). Nous organisons également en collaboration avec le BES des visites de notre campus de la Plaine.</p>
                    <p>Le jour de la rentrée, un barbecue est également organisé pour que les BA1 puissent avoir un premier contact avec les étudiants de leur classe, d’années supérieures, d’autres sections ainsi qu’avec les membres du comité de cercle.</p>
                    <p><strong>Retrouvez toutes les photos de l'édition 2017 <a target="_blank" href="https://www.facebook.com/media/set/?set=a.1540118062715679.1073742021.100001524998304&type=3"> ici</a>.</strong></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Les 
                        <strong>Recueils</strong>
                    </h2>
                    <hr>
                    <p>Chaque été, les délégués de section s’organisent avec les professeurs pour avoir les examens (et leurs corrigés) de l’année venant de s’écouler. L’objectif est de pouvoir compléter le recueil d’examens d’année en année. Il s’agit d’un outil extrêmement utile à l’approche du blocus/des examens car il permet de se rendre compte de la différence entre le niveau d’un examen en secondaire et à l’université.</p>
                    
                    <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyWE5jYmVSc2llczg/view?usp=sharing"><img class="img-responsive" src="../img/receuil/agro.png" alt=""></a>
                    <h3>Agro</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyWE5jYmVSc2llczg/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyX0JkOEdTMTI3WVE/view?usp=sharing"><img class="img-responsive" src="../img/receuil/bio.png" alt=""></a>
                    <h3>Bio</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyX0JkOEdTMTI3WVE/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyaXZHaXptWXB4V1E/view?usp=sharing"><img class="img-responsive" src="../img/receuil/chimie.png" alt=""></a>
                    <h3>Chimie</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyaXZHaXptWXB4V1E/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyU2JmaTJ4ejNoODA/view?usp=sharing"><img class="img-responsive" src="../img/receuil/geog.png" alt=""></a>
                    <h3>Géographie</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyU2JmaTJ4ejNoODA/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyaUtIbHY0cUp4Qms/view?usp=sharing"><img class="img-responsive" src="../img/receuil/geol.png" alt=""></a>
                    <h3>Géologie</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyaUtIbHY0cUp4Qms/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyaXBtY2NNcHMxYnc/view?usp=sharing"><img class="img-responsive" src="../img/receuil/math.png" alt=""></a>
                    <h3>Math</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyaXBtY2NNcHMxYnc/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyUE4wX0dKa1EyYUU/view?usp=sharing"><img class="img-responsive" src="../img/receuil/physique.png" alt=""></a>
                    <h3>Physique</br>
                    <small><a target="_blank" href="https://drive.google.com/file/d/0BwNlRwzDL9lyUE4wX0dKa1EyYUU/view?usp=sharing">Visionner</a></small>
                    </h3>
                </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Le 
                        <strong>Parrainage Social</strong>
                    </h2>
                    <hr>
                    <p>Le parrainage social est une activité d'entraide que nous organisons chaque année aux environs de la mi-octobre. Le but est que chaque étudiant de BA1 puisse se trouver un parrain d'une année supérieure dans sa propre section.</p>
                    <p>Il pourra alors bénéficier des résumés, notes et surtout des petits conseils de ce dernier. Cela crée également un lien, parfois solide, dès les débuts de l'étudiant à l'université et peut faciliter son intégration.</p>
                    <p>L'organisation du parrainage connait de plus en plus de modifications ces derniers temps. En effet, bien que nous puissions l'organiser seuls, nous tentons d'en faire une activité facultaire en réunissant l'ensemble des acteurs de la Faculté des Sciences, associations étudiantes, A.Sc.Br., assistants, enseignants, anciens. Le système de parrainage change lui aussi.</p>
                    <p>Alors que nous nous basions sur le canevas « un parrain + un filleul », les nouveaux étudiants seront désormais accueillis dans des groupes encadrés par plusieurs parrains. Ceci traduit une volonté de toujours mieux faire et de faciliter la réussite en BA1. Ils pourront eux aussi devenir parrains par la suite.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">La 
                        <strong>Soirée de rentrée</strong>
                    </h2>
                    <hr>
                    <p>La soirée de rentrée du Cercle des Sciences fut organisée pour la première fois en 2010, dans le but de réunir l’ensemble des étudiants de la faculté des Sciences, et ce, des premières années jusqu’aux doctorants.</p>
                    <p>Toujours organisée près  des campus universitaires (du Solbosch et de la Plaine), cette soirée offre une occasion pour les étudiants de première année de fêter leur arrivée au sein de l’université.</p>
                    <p>Son cadre beaucoup plus intime que celui de la Nuit des Sciences permet une plus grande proximité et des échanges différents entre les étudiants, toutes sections de la Faculté confondues. De plus, elle donne la possibilité d’apprendre à se connaître autrement que devant des syllabus.</p>
                    <p>Sans code vestimentaire particulier, la soirée de rentrée permet de se retrouver peu après la sortie des cours. Chaque année, un thème y est proposé, renforçant ainsi l’atmosphère de détente et de fête recherchée lors de cette soirée.</p>
                    <p><strong> Retrouvez les photos de l'édition 2017 <a target="_blank" href="https://www.facebook.com/photos.cds/media_set?set=a.1544314368962715.1073742024.100001524998304&type=3">ici</a> !</strong></p>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <?php include("include/footer.php"); ?>

</body>

</html>
