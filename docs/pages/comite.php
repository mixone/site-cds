<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php include("include/head.php"); ?>
        <style>
        .btn:hover {
            background-color: purple;
            color: white;
        }
        </style>
    </head>
    <body>
        <!-- Navigation -->
        <?php include_once("pages/analyticstracking.php") ?>
        <?php include("include/navbar.php"); ?>
        <div class="container">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-lg" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Comité 2018 - 2019
                    </button>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  
                              
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">Comité
                            <strong>2018 - 2019</strong>
                        </h2>
                        <hr>
                    </div>
                    <!--
                    <div class="col-md-6">
                        <img class="img-responsive img-border-center" src="../img/delegue/bureau.jpg" alt="">
                    </div>
                    -->
                    <div class="col-md-6">
                        <p>Le bureau du Cercle des Sciences vous présente son comité pour l'année 2018 - 2019 !</p>
                        <p>Le bon fonctionnement de notre cercle est uniquement rendu possible grâce au travail et au dévouement de son comité. Vous retrouverez ci-dessous le détail de l'ensemble des cellules constituant le cercle ainsi que les différents délégués responsables de ces dernières. Vous pouvez dorénavant trouver le résumé des réunions.</p>
                        <p>Après chaque réunion nous mettons en ligne un PV de cette dernière que vous pouvez retrouver <a href="pvs.php">ici</a>.</p>
                        <p>Grandissimement Vôtre !</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">Le
                            <strong>Bureau</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/president.jpg" alt="">
                        <h3>Sébastien Verkercke
                            <small>Président</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/vpi.jpg" alt="">
                        <h3>Coraline Mulder
                            <small>VPI</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/vpe.jpg" alt="">
                        <h3>Florian Belot
                            <small>VPE</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/vpc.jpg" alt="">
                        <h3>Clara Hody
                            <small>VPC</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/tres.jpg" alt="">
                        <h3>Thomas Fontaine
                            <small>Trésorier</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/ff.jpg" alt="">
                        <h3>Adam Bigaj
                            <small>F&F</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/prezb.jpg" alt="">
                        <h3>Benjamin Marotte
                            <small>PDB</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">La
                            <strong>Cellule Interne</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/bar1.jpg" alt="">
                        <h3>Mohammed Ben Haddou
                            <small>Délégué Bar</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/bar2.jpg" alt="">
                        <h3>Mathieu Remy
                            <small>Délégué Bar</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/bar3.jpg" alt="">
                        <h3>Noëmie Muller
                            <small>Déléguée Bar</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/bar4.jpg" alt="">
                        <h3>Cyprien Hoffman
                            <small>Délégué Bar</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/bouffe2.jpg" alt="">
                        <h3>Pawel Krynski
                            <small>Délégué BA</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/eco-resp.jpg" alt="">
                        <h3>Sylvain Nowé
                            <small>Eco-Resp</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/chant.jpg" alt="">
                        <h3>Colin Sterckx
                            <small>Délégué Chant</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">La
                            <strong>Cellule Communication</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/pca.jpg" alt="">
                        <h3>Nikita Buch
                            <small>Déléguée PCA</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/prom.jpg" alt="">
                        <h3>Lucie Bazantay
                            <small>Déléguée Prom</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/visu1.jpg" alt="">
                        <h3>Sarah Laurent
                            <small>Déléguée Visuel</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/confact.jpg" alt="">
                        <h3>Adrien Bouhy
                            <small>Délégué Confact</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/phys.jpg" alt="">
                        <h3>Jérémy Broissin
                            <small>Délégué Physique</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/chimie.jpg" alt="">
                        <h3>Nathan Goffart
                            <small>Délégué Chimie</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/bio.jpg" alt="">
                        <h3>Bartholomeo Bouteiller
                            <small>Délégué Bio</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/agro.jpg" alt="">
                        <h3>Lucie Haemers
                            <small>Déléguée Agro</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/geo.jpg" alt="">
                        <h3>Julia Ferreira Lerche
                            <small>Déléguée Géographie/Géologie</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/cultureSocial.jpg" alt="">
                        <h3>Louis Coeugniet
                            <small>Culture-Social</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/librex.jpg" alt="">
                        <h3>Matéo Yerlès
                            <small>Conf-Librex</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/info.jpg" alt="">
                        <h3>Miguel Terol Espino
                            <small>Délégué Web-Info</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">La
                            <strong>Cellule Externe</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/balev12.jpg" alt="">
                        <h3>Géraldine Houssa
                            <small>Déléguée Balev</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/balev2.jpg" alt="">
                        <h3>Lucie Rohart
                            <small>Déléguée Balev</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/sports1.jpg" alt="">
                        <h3>Terence Vito de Buijl
                            <small>Sports</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/sports2.jpg" alt="">
                        <h3>Alexis Giaprakis
                            <small>Sports</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/sports-event1.jpg" alt="">
                        <h3>Quentin Murati
                            <small>Sports-Events</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/ffsb1.jpg" alt="">
                        <h3>Omaïma Adaoudi
                            <small>Déléguée FFSB</small>
                        </h3>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/jobday1.jpg" alt="">
                            <h3>Sylvain Kabbadj
                                <small>Délégué Jobday</small>
                            </h3>
                        </div>
                        <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/jobday2.jpg" alt="">
                            <h3>Max Vanden Bemden
                                <small>Délégué Jobday</small>
                            </h3>
                        </div>
                        <div class="clearfix">
                            <div class="col-lg-12">
                                <hr>
                                <h2 class="intro-text text-center">La
                                    <strong>Cellule Prez'</strong>
                                </h2>
                                <hr>
                            </div>
                            <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/vieux12.jpg" alt="">
                                <h3>Jules Talbotier
                                    <small>AScBr-Vieux</small>
                                </h3>
                            </div>
                            <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/vieux2.jpg" alt="">
                                <h3>Loic Larroumets
                                    <small>AScBr-Vieux</small>
                                </h3>
                            </div>
                            <div class="col-sm-4 text-center">
                        <img class="img-responsive" src="../img/delegue/vts1.jpg" alt="">
                                <h3>Chems Mabrouk
                                    <small>VTS</small>
                                </h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                </div> 
                </div>
                <!-- /.card 2018-2019 -->
                <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-lg" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                      Comité 2017 - 2018
                    </button>
                  </h5>
                </div>

                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  
                              
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">Comité
                            <strong>2017 - 2018</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">Le
                            <strong>Bureau</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Thibaut Kemajou
                            <small>Président</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Jules Talbotier
                            <small>VPI</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Sébastien Verkercke
                            <small>VPE</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Renaud Gaban
                            <small>VPC</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Adam Bigaj
                            <small>Trésorier</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Guillaume Andolina
                            <small>F&F</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Victor Nocent
                            <small>PDB</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">La
                            <strong>Cellule Interne</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Antonin Vital
                            <small>Délégué Bar</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Sébastien Dechamps
                            <small>Délégué Bar</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Marine Anzalone
                            <small>Déléguée Bar</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Sarah Zeghlache
                            <small>Délégué BA</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Sarah Gravier
                            <small>Eco-Resp</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Emilie Gérouville
                            <small>Déléguée Chant</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">La
                            <strong>Cellule Communication</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Nikita Buch
                            <small>Déléguée PCA</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Sylvain Kabbadj
                            <small>Délégué Prom</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>spiridon ciupitu
                            <small>Délégué Visuel</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Clara Hody
                            <small>Déléguée Confact</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Pawel Krynski
                            <small>Délégué Physique</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Colin Sterckx
                            <small>Délégué Math</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Jean Gillet
                            <small>Délégué Chimie</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Christian Coremans
                            <small>Délégué Bio</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Yonas Roza Perez
                            <small>Délégué Agro</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Zakari Semal
                            <small>Délégué Géographie</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Nicolas Henry
                            <small>Culture-Social</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Louis de Cléty
                            <small>Conf-Librex</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Nathan Liccardo
                            <small>Délégué Web-Info</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Sylvain Nowé
                            <small>Délégué Géologie</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">La
                            <strong>Cellule Externe</strong>
                        </h2>
                        <hr>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Florian Belot
                            <small>Délégué Balev</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Kevin Theys
                            <small>Délégué Balev</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Max Vanden Bemeden
                            <small>Sports</small>
                        </h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Samuel Bier
                            <small>Sports</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Adrien Bouhy
                            <small>Sports-Events</small>
                        </h3>
                    </div>
                    <div class="col-sm-4 text-center">
                        
                        <h3>Elena Acedo Reina
                            <small>Déléguée FFSB</small>
                        </h3>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-4 text-center">
                            
                            <h3>Nicolas Vriamont
                                <small>Délégué Jobday</small>
                            </h3>
                        </div>
                        <div class="col-sm-4 text-center">
                            
                            <h3>Aude Bardela
                                <small>Déléguée Jobday</small>
                            </h3>
                        </div>
                        <div class="clearfix">
                            <div class="col-lg-12">
                                <hr>
                                <h2 class="intro-text text-center">La
                                    <strong>Cellule Prez'</strong>
                                </h2>
                                <hr>
                            </div>
                            <div class="col-sm-4 text-center">
                                
                                <h3>Jolan Vancoppenolle
                                    <small>AScBr-Vieux</small>
                                </h3>
                            </div>
                            <div class="col-sm-4 text-center">
                                
                                <h3>Virginie Martinez Toledo
                                    <small>AScBr-Vieux</small>
                                </h3>
                            </div>
                            <div class="col-sm-4 text-center">
                                
                                <h3>Loïc Larroumets
                                    <small>VTS</small>
                                </h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                </div> 
                </div>
                <!-- /.card 2017-2018 -->
            </div>
            <!-- /.accordion -->
        </div>
        <!-- /.container -->
        <script>
        function clickFirst(){
            $("#accordion")[0].children[0].children[0].children[0].children[0].click(); 
        }
        window.onload = clickFirst;
        </script>
        <?php include("include/footer.php"); ?>
    </body>
</html>
