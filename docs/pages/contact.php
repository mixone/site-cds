<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("include/navbar.php"); ?>

    <!-- Container -->
    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Ou <strong>nous trouver</strong></h2>
                    <hr>
                </div>
                <div class="col-lg-12">
                    <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD-s0bqByysKh7DGaTKMvEsKn5PtOvet4w&amp;q=Cercle+des+Sciences+de+l&#39;ULB"></iframe>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
   
    <!-- Footer -->
    <?php include("include/footer.php"); ?>

</body>

</html>
