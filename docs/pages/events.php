<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("include/navbar.php"); ?>


    <!-- Container -->
    <?php
        $events = 5; 
        echo "<div class='container'>"; 
        while ($events > 0){
            include('events/event'.$events.'.php'); 
            $events = $events-1;
        }
        echo "</div>";
    ?>


    <!-- Footer -->
    <?php include("include/footer.php"); ?>

</body>

</html>