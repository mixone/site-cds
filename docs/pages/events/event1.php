<div class="row" id="ffsb">
<div class="box">
<div class="col-lg-12">
<hr>
<h2 class="intro-text text-center">Le <strong>ffsb</strong></h2>
<hr>
<img class="img-responsive img-full" src="../img/slides/slide-4.jpg" alt="">
<hr class="visible-xs">
</br>
<p>Le Festival du Film Scientifique de Bruxelles a comme devise « la Science pour tous ». Cet évènement d'une semaine vise un public composé à la fois de scientifiques aguerris et de simples curieux. Les projections de documentaires en soirée sont précédées de rencontres avec des doctorants venus présenter leur recherche et sont suivies de débats animés par des spécialistes, souvent professeurs à l'ULB. Le Festival accueille également de nombreux élèves du secondaire, venus avec leurs professeurs.</p>
<p>Avec une moyenne de plus de 200 élèves et 100 à 200 personnes par projection, ce sont plus de 1000 personnes qui viennent apprendre, découvrir et se divertir au FFSB. Vous pouvez également retrouver les projections précédentes ainsi que les détails concernants le prochain programme en suivant <a href="http://www.ffsbxl.be" onclick="window.open(this.href); return false;">ce lien</a>.</p>
<p><strong>Le FFSB a lieu chaque année en même temps que le printemps des sciences ( <a href="https://www.facebook.com/events/897354447092309/" onclick="window.open(this.href); return false;">infos</a> ).</strong></p>
</div>
</div>
</div>