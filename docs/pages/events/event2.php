<div class="row" id="kmUlb">
<div class="box">
<div class="col-lg-12">
<hr>
<h2 class="intro-text text-center">Les 
    <strong>10km de l'ULB</strong>
</h2>
<hr>
<img class="img-responsive img-full" src="../img/slides/slide-3.jpg" alt="">
<hr class="visible-xs">
</br>
<p>Les 10 km de l'ULB sont une course philanthropique, dont tous les bénéfices sont reversés à la recherche scientifique de la Faculté des Sciences de l'ULB. A titre d'exemple, la première édition a permis de financer 5 projets à hauteur de 10 000€.</p>
<p>Rassemblant plus d'un millier de personnes, l'activité se veut tout public. Du coureur occasionnel au sportif confirmé désireux d’entamer sa saison calmement en prévision des 20 km de Bruxelles, la cible est on ne peut plus large et les familles des participants ne sont pas en reste. Elles profitent du barbecue ou des divertissements du « Village Sponsor » en attendant les résultats de la grande tombola.</p>
<p>Vous pouvez dès à présent retrouver l'ensemble des informations concernant les futures éditions mais également des éditions passées en suivant <a href="http://www.10kmulb.org" onclick="window.open(this.href); return false;">ce lien</a>.</p>
<p><strong>En espérant vous y voir nombreux et en pleine forme pour l'édition 2018 !</strong></p>
</div>
</div>
</div>