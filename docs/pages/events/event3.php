<div class="row" id="jobday">
<div class="box">
<div class="col-lg-12">
<hr>
<h2 class="intro-text text-center">Le 
    <strong>Jobday</strong>
</h2>
<hr>
<img class="img-responsive img-full" src="../img/slides/slide-0.jpg" alt="">
<hr class="visible-xs">
</br>
<p>Le Forum de l’Emploi est une journée de rencontre entre les étudiants en sciences et les entreprises. Cet événement a pour objectif d’instaurer une dynamique d’échange et de communication entre le monde universitaire et celui de l’entreprise mais également de préparer les étudiants au marché du travail et à la recherche d’un emploi.</p>
<p>Le public cible est composé d'étudiants en Master et de doctorants en sciences, sciences pharmaceutiques et sciences de l’ingénieur à orientation bioingénieur. Un CV-Book reprend les références des étudiants en fin de cycle, l’objectif étant de leur permettre de décrocher un entretien en vue d’un stage ou d’un emploi.</p>
<p>Il constitue également une opportunité pour les entreprises de rentrer en contact avec le corps académique de la Faculté, ce qui est idéal pour entamer des projets de partenariat université-privé. Vous trouverez toutes les informations concernant les précédentes éditions sur <a target="_blank" href="http://www.jobday-sciences.be/jobday/index.html">cette page</a>.</p>
<p><strong>Cet évènement a généralement lieu durant la deuxième partie de l'année académique.</strong>
</div>
</div>
</div>