<div class="row" id="nds">
<div class="box">
<div class="col-lg-12">
<hr>
<h2 class="intro-text text-center">La 
    <strong>Nuit des sciences</strong>
</h2>
<hr>
<img class="img-responsive img-full" src="../img/slides/slide-1.jpg" alt="">
<hr class="visible-xs">
</br>
<p>La Nuit des Sciences est le bal officiel de la Faculté des Sciences, organisé par le Cercle des Sciences. Cette Nuit réunit chaque année près de 1000 personnes, issues de notre Faculté mais également de toute l’ULB.</p> 
<p>Précédée d’un banquet, elle est l’occasion unique de sortir ses plus belles robes ou ses plus beaux costumes. C'est aussi le moment idéal pour faire de nouvelles rencontres : étudiants, professeurs et doctorants se fréquentent alors dans un tout autre contexte que les cours ou le travail.</p>
<p> Nous essayons de varier les salles et les ambiances (musique, décoration, etc) d’année en année, afin que la lassitude ne gagne pas les habitués. Les services mis à disposition sont nombreux : navettes Solbosch-bal, vestiaires, parking (selon les possibilités), terminaux Bancontact (selon les possibilités), etc pour vous offrir la plus inoubliable des soirées !</p>
<p><strong>Les informations concernant la 82e Nuit des Sciences sont disponibles : <a href="https://www.facebook.com/events/570881746591433/" onclick="window.open(this.href); return false;">infos</a>.</strong></p>
</div>
</div>
</div>