<div class="row" id="gazon">
<div class="box">
<div class="col-lg-12">
<hr>
<h2 class="intro-text text-center">Le 
    <strong>Gazon</strong>
</h2>
<hr>
<img class="img-responsive img-full" src="../img/slides/slide-2.jpg" alt="">
<hr class="visible-xs">
</br>
<p>La journée Gazon du Cercle des Sciences évolue d'année en année. Si l’objectif reste fondamentalement identique, à savoir offrir un après-midi de loisirs et de détente aux étudiants après les examens de juin, les moyens mis en œuvre ont fortement évolué en 10 ans et l’évènement ne cesse de gagner de l'ampleur. En effet, l'édition 2009 n'était qu'un « simple » barbecue sportif mais fil des années, diverses autres activités sont également venu s'ajouter, permettant ainsi aux étudiants d'occuper leur après-midi comme il se doit ! </p> 
<p>Bien que l'activité soit à but purement festif, elle n'en est pas moins une activité extrêmement sérieuse dans sa préparation. Elle requiert en effet la mobilisation de l'ensemble du comité pendant plusieurs jours !</p>
<p><strong>Vous pouvez dès à présent consulter les photos sur Facebook du <a target="_blank" href="https://www.facebook.com/media/set/?set=a.1467150440012442.1073742019.100001524998304&type=3">Gazon</a> et du <a target="_blank" href="https://www.facebook.com/media/set/?set=a.1466296933431126.1073742018.100001524998304&type=3">TD</a> en attendant la prochaine édition !</strong></p>
</div>
</div>
</div>