<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"/>
                    <p>Cercle des Sciences 2017 - 2018 |
                    	<a style="color:inherit;
								  text-decoration: none;" href="https://www.facebook.com/groups/4741652051"><i class="fa fa-facebook"></i></a>
						<a style="color:inherit;
								  text-decoration: none;" href="https://www.instagram.com/cercle.des.sciences/"><i class="fa fa-instagram"></i></a></p>
                </div>
            </div>
        </div>
</footer>

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

<!-- Script to Activate the Carousel -->
<script>
  $('.carousel').carousel({
    interval: 5000 //changes the speed
  })
</script>