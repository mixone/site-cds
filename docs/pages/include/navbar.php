<div class="brand">Cercle des Sciences</div>
<div><a href="/pages/contact.php" class="address-bar">Avenue Franklin Roosevelt | Bruxelles, 1050 | 125 CP 160/31</a></div>

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
            <a class="navbar-brand" href="/index.php">Cercle des Sciences</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/index.php">Accueil</a>
                </li>
                <li>
                    <a href="/pages/ba1.php">BA1</a>
                </li>
                <li>
                    <a href="/pages/comite.php">Comité</a>
                </li>
                <li>
                    <a href="/pages/events.php">Events</a>
                </li>
                <li>
                    <a href="/pages/agenda.php">Agenda</a> 
                </li>
                <li>
                    <a href="/pages/prom.php?id=5">Prométhée</a>
                </li>
                <!--<li>
                    <a href="/pages/calendrier.php">Calendrier</a>
                </li>
                <li>
                    <a href="/pages/contact.php">Contacts</a>
                </li>-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
