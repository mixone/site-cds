<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("include/navbar.php"); ?>


    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Le <strong>Prométhée</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-lg-12">
                    <img class="img-responsive img-center" src="../img/images/prom.png" alt="">
                </div>
                <div class="clearfix"></div>
				</br>
           		<p>Le Prométhée est le journal officiel du Cercle des Sciences. Ce journal est écrit par des étudiant•e•s du Comité de Cercle pour un public large. Ce journal se veut scientifique et sérieux. En plus des articles scientifiques, vous trouverez également dans le Prométhée des articles éthiques et, parfois, engagés.</p>
            	<p>Les journaux seront distribués, sous format papier, lors de gros événements du Cercle des Sciences. Ils seront également disponibles en ligne, sur ce site, ainsi que sur la page Facebook du Prométhée : Prométhée CdS. En plus des Prométhée, nous vous proposons également des articles hebdomadaires sur la page Facebook.</p>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>2017 - 2018</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/sh/fg0uffpdbosd5mz/AACnsiK9RZVxunbldjjKYGUYa?dl=0&preview=Prométhée+Gazon+2017.pdf"><img class="img-responsive" src="../img/prom/prom1.png" alt=""></a>
                    <h3>Prométhée Gazon</br>
                    <small><a target="_blank" href="https://www.dropbox.com/sh/fg0uffpdbosd5mz/AACnsiK9RZVxunbldjjKYGUYa?dl=0&preview=Prométhée+Gazon+2017.pdf">Visionner</a></small></h3>
                </div>
                
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/s/s2r8ivmkmsyiiuc/Prométhée+JANE+2017.pdf"><img class="img-responsive" src="../img/prom/Jane2017.png" alt=""></a>
                    <h3>Prométhée Jane</br>
                    <small><a target="_blank" href="https://www.dropbox.com/s/s2r8ivmkmsyiiuc/Prométhée+JANE+2017.pdf">Visionner</a></small></h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/s/vpmwkcukpcdgl65/Prométhée+Saint-Verhaegen+2017.pdf"><img class="img-responsive" src="../img/prom/STV2017.png" alt=""></a>
                    <h3>Prométhée Saint-V</br>
                    <small><a target="_blank" href="https://www.dropbox.com/s/vpmwkcukpcdgl65/Prométhée+Saint-Verhaegen+2017.pdf">Visionner</a></small></h3>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/s/e0cj7jhtzx5j0ke/Prom%C3%A9th%C3%A9e%20FFSB%202018.pdf"><img class="img-responsive" src="../img/prom/FFSB2018.png" alt=""></a>
                    <h3>Prométhée FFSB</br>
                    <small><a target="_blank" href="https://www.dropbox.com/s/e0cj7jhtzx5j0ke/Prom%C3%A9th%C3%A9e%20FFSB%202018.pdf">Visionner</a></small></h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/s/5fk6vi73oxzgww5/Prom%C3%A9th%C3%A9e%20AG%20de%20fin%20de%20mandat%202018.pdf"><img class="img-responsive" src="../img/prom/AG2018.png" alt=""></a>
                    <h3>Prométhée AG</br>
                    <small><a target="_blank" href="https://www.dropbox.com/s/5fk6vi73oxzgww5/Prom%C3%A9th%C3%A9e%20AG%20de%20fin%20de%20mandat%202018.pdf">Visionner</a></small></h3>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>2016 - 2017</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+Gazon+2016.pdf"><img class="img-responsive" src="../img/prom/Gazon2016.png" alt=""></a>
                    <h3>Prométhée Gazon</br>
                    <small><a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+Gazon+2016.pdf">Visionner</a></small>
                    </h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+JANE+2016.pdf"><img class="img-responsive" src="../img/prom/Jane2016.png" alt=""></a>
                    <h3>Prométhée Jane</br>
                    <small><a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+JANE+2016.pdf">Visionner</a></small>
                    </h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+Banquet+2016.pdf"><img class="img-responsive" src="../img/prom/Banquet2016.png" alt=""></a>
                    <h3>Prométhée Banquet</br>
                    <small><a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+Banquet+2016.pdf">Visionner</a></small>
                    </h3>
                </div>
                <div class="clearfix"></div>

                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+Bal+2017.pdf"><img class="img-responsive" src="../img/prom/Bal2017.png" alt=""></a>
                    <h3>Prométhée Bal</br>
                    <small><a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+Bal+2017.pdf">Visionner</a></small></h3>
                </div>
                <div class="col-sm-4 text-center">
                    <a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+AG+2017.pdf"><img class="img-responsive" src="../img/prom/AG2017.png" alt=""></a>
                    <h3>Prométhée AG</br>
                    <small><a target="_blank" href="https://www.dropbox.com/sh/3h8o3ivsjmpn5hg/AACWnQl9YhaF6lWh964mad_Qa?dl=0&preview=Prométhée+AG+2017.pdf">Visionner</a></small></h3>
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>
    <!-- /.container -->

    <?php include("include/footer.php"); ?>

</body>

</html>
