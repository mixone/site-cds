<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include("include/head.php"); ?>
</head>

<body>

    <!-- Navigation -->
    <?php include_once("pages/analyticstracking.php") ?>
    <?php include("include/navbar.php"); ?>


    <div class="container">
        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Les <strong>Procès Verbaux</strong>
                    </h2>
                    <hr>
                </div>
                <p>Le Comité du Cercle des Sciences se réunit à une fréquence d'une fois par semaine afin de discuter de ses activités à organiser et de débriefer sur les événements passés. Ces réunions sont également un moyen pour le bureau de communiquer à tout son comité les dernières nouvelles à savoir au sujet des prises de décisions de l'ACE, des informations importantes à savoir qui les concernent directement, et bien d'autres encore.</p>

				<p>Les réunions sont obligatoires pour le Comité de Cercle et ouvertes au public intéressé. Elles ont pour le moment lieu le mardi.</p>

           		<p>Vous trouverez ci-dessous les PV de toutes les réunions ayant eu lieu depuis le début du mandat 2017-2018.</p>
            </div>
		</div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        Les <strong>Procès Verbaux</strong>
                    </h2>
                    <hr>
                </div>

                <?php
                    foreach(file('pvs/dates.txt') as $line) {
                        echo "<div class='col-sm-4 text-center'>";
                        echo "<a target='_blank' href='pvs/".$line.".pdf'>";
                        echo "<img class='img-responsive' src='pvs/".$line."-image.png' alt=''></a>";
                        echo "<h3>Reunion du ".$line."</br>";
                        echo "<small><a target='_blank' href='pvs/".$line.".pdf'>Visionner</a></small></h3>";
                        echo "</div>";
                    }
                ?>

            </div>
        </div>
    </div>
    <!-- /.container -->

    <?php include("include/footer.php"); ?>

</body>

</html>
